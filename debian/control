Source: r-bioc-mutationalpatterns
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-genomicranges,
               r-cran-nmf,
               r-bioc-s4vectors,
               r-bioc-biocgenerics,
               r-bioc-bsgenome,
               r-bioc-variantannotation,
               r-cran-dplyr,
               r-cran-tibble,
               r-cran-purrr,
               r-cran-tidyr,
               r-cran-stringr,
               r-cran-magrittr,
               r-cran-ggplot2,
               r-cran-pracma,
               r-bioc-iranges,
               r-bioc-genomeinfodb,
               r-bioc-biostrings,
               r-cran-ggdendro,
               r-cran-cowplot,
               r-cran-ggalluvial,
               r-cran-rcolorbrewer
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-mutationalpatterns
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-mutationalpatterns.git
Homepage: https://bioconductor.org/packages/MutationalPatterns/
Rules-Requires-Root: no

Package: r-bioc-mutationalpatterns
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R comprehensive genome-wide analysis of mutational processes
 This BioConductor package provides an extensive toolset for the
 characterization and visualization of a wide range of mutational patterns
 in base substitution catalogs.
